class Stack():
    def __init__(self):
        """ Создаем пустой стек """
        self.items = []

    def push(self, item):
        """ Введение элемента в стек """
        self.items.append(item)

    def pop(self):
        """ Удаление из стека последнего элемента """
        return self.items.pop()

    def is_empty(self):
        """ Проверка стека на заполненность - True, если пустой """
        return self.items == []

    def peek(self):
        """ Вывод самого верхнего элемента в стеке """
        if not self.is_empty():
            return self.items[-1]

    def get_stack(self):
        """ Вывод содержимого стека """
        return self.items


s = Stack()
print(s.is_empty())  # Выводится True, т.к. стек пуст
s.push("A")
s.push("B")
s.push("C")
print(s.get_stack())  # Выводится ['A', 'B', 'C'], где С последний элемент
print(s.peek())  # Выводится C, т.к. сейчас он последний элемент в стеке
s.pop()
print(s.get_stack())  # Выводится ['A', 'B'], т.к. извлечён последний элемент в стеке
print(s.is_empty())  # Выводится False, т.к. стек не пуст
print(s.peek())  # Выводится B, т.к. теперь он последний элемент в стеке
s.pop()
s.pop()
print(s.is_empty())  # Снова выводится True, т.к. стек снова пуст
