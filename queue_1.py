class Queue:
    def __init__(self):
        """ Создание пустой очереди """
        self.queue = []

    def add(self, element):
        """ Вставка элемента в очередь """
        self.queue.insert(0, element)

    def remove(self):
        """ Удаление элемента из очереди """
        return self.queue.pop()

    def size(self):
        """ Отображение размера очереди """
        return len(self.queue)

    def is_empty(self):
        """ Проверка на то - пуста ли очередь, True если да """
        return self.queue == []

    def get_queue(self):
        """ Вывод элементов очереди """
        return self.queue


n = Queue()
print(n.is_empty())  # Выводится True, т.к. очередь пуста
print(n.size())  # Выводится 0, т.к. в очереди нет элементов

n.add("A")
n.add("B")
n.add("C")
n.add("D")
print(n.get_queue())  # Выводится ['D', 'C', 'B', 'A']
print(n.size())  # Выводится 4, т.к. в очереди 4 элемента

n.remove()
n.remove()
print(n.get_queue())  # Выводится ['D', 'C']

print(n.is_empty())  # Выводится False, т.к. очередь не пуста

print(n.size())  # Выводится 2, т.к. в очереди 2 элемента

n.remove()
print(n.get_queue())  # Выводится ['D']

n.remove()
print(n.size())  # Выводится 0, т.к. элементов в очереди больше нет
print(n.is_empty())  # Выводится True, т.к. очередь теперь пуста
print(n.get_queue())  # Выводится [], т.к. в очереди нет элементов
