import json
import requests


response = requests.get("http://iss.moex.com/iss/securities.json")
exchange_json = json.loads(response.text)

securities = exchange_json['securities']
metadata = securities['metadata']
columns = securities['columns']
data = securities['data']

companies_list = []

for i in range(len(data)):
    companies_list.append(data[i][4])

print(companies_list)
