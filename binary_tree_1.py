class BinaryTree:
    def __init__(self, node=None, left=None, right=None):
        self.node = node
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.node)

    def search(self, node, target):
        """ Обход бинарного дерева """
        if node is None:
            return 0
        else:
            if target == node.node:
                return 1
            if target < node.node:
                return self.search(node.left, target)
            else:
                return self.search(node.right, target)


tree = BinaryTree("Root")  # Корень дерева
BranchA = BinaryTree("Branch A")  # Имя ветви A
BranchB = BinaryTree("Branch B")  # Имя ветви B
tree.left = BranchA  # Ветвь, растущая влево
tree.right = BranchB  # Ветвь растущая вправо

LeafC = BinaryTree("Leaf C")  # Имя листа C
LeafD = BinaryTree("Leaf D")  # Имя листа D
LeafE = BinaryTree("Leaf E")  # Имя листа E
LeafF = BinaryTree("Leaf F")  # Имя листа F
BranchA.left = LeafC  # Лист C ветви A
BranchA.right = LeafD  # Лист D ветви A
BranchB.left = LeafE  # Лист E ветви B
BranchB.right = LeafF  # Лист F ветви B


def show_tree(self):
    """ Функция рекурсивного обхода дерева с выводом всех ветвей и листьев дерева """
    if self.left is not None:
        show_tree(self.left)
    if self.right is not None:
        show_tree(self.right)
    print(self.node)


show_tree(tree)
# Выводится дерево
# Leaf C
# Leaf D
# Branch A
# Leaf E
# Leaf F
# Branch B
# Root

